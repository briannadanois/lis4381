# LIS4381 - Mobile Web Application Development

## LIS4381 Requirements:

###Course Work Links:

###A1 README.md
1.[A1 README.md](a1/README.md "My A1 README.md file")

    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First Application
    * Provide Screenshots of Installations
    * Create Bitbucket repo
    * Complete Bitbucket Tutorials
    * Provide Git Command Descriptions
    
###A2 README.md
2.[A2 README.md](a2/README.md "My A2 README.md file")
    
    * Create a mobile recipe app using Android Studio.

###A3 README.md
3.[A3 README.md](a3/README.md "My A3 README.md file")
    
    * Create a mobile event calculator app using Android Studio.
    * Create a database for petstores, pets and customers using MySQL

###P1 README.md
4.[P1 README.md](p1/README.md "My P1 README.md file")
    
    * Develop a mobile app business card promoting myself and my skills, experience and education
    * Implement features including buttons, drop shadows and borders.

###A4 README.md
5.[A4 README.md](a4/README.md "My A4 README.md file")
    
    * Developed a Mobile First website out of a Bootstrap template.
    * Implement features including an active sliding carousel, images and links.
    * Implement a client-side verification form system.

###A5 README.md
6.[A5 README.md](a5/README.md "My A5 README.md file")

    * Implement a server-side verification form system.
    * Verify mysql database connection

###P2 README.md
7.[P2 README.md](p2/README.md "My P2 README.md file")

    * Reinforce a server-side verification form system.
    * Verify mysql database connection
    * Enable Delete Option to the Database from the Web Form
    * Enable Edit Option to the Database from the Web Form
    * Enable the Data Already Defined in the Database to Populate in the Web Form for the Edit Feature
    * Set up and RSS Feed