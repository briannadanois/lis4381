<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
		<link rel="icon" href="../favicon.ico">
    <!-- <link rel="icon" href="favicon.ico"> -->

		<title>LIS4381 - Assignment3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("../global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Assigment 3 consisted of creating another mobile application but for concert tickets. This time we also worked with mysql to familiarize ourselves with working client and server side.
				</p>

				<h4>Concert Interface One</h4>
				<img src="a3.png" class="img-responsive center-block" alt="concert image with a drop down menu to select a band">

				<h4>Concert Interface Two</h4>
				<img src="a3-1.png" class="img-responsive center-block" alt="same interface except with the payment amount necessary after the user presses the purchase button">
				
				<h4>Concert Interface Two</h4>
				<img src="a3-erd.png" class="img-responsive center-block" alt="mysql tables displaying petstores">
				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
