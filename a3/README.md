#Brianna Danois

#LIS4381 - Mobile Web Application Development 

##Assignment 3 README.md

####Screenshot of ERD

![MySQL ERD](a3-erd.png)

####Screenshot of First User Interface

![Tickets](a3.png)

####Screenshot of First User Interface

![Tickets](a3-1.png)

*Link to A3 MyWorkBench File*
* [A3 Link to A3.mwb](a3.mwb "A3 MyWorkBench Link")

*Link to Assignment 3 SQL File*
* [A3: SQL File](a3.sql "A3: Assignment 3 SQL File")