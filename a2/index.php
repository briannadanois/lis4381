<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="../favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("../global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Assignment 2 involved working more in depth with android studio to actually make an interactive application displaying a recipe for Bruschetta. 
				</p>

				<h4>Bruschetta Interface One</h4>
				<img src="a2-1.png" class="img-responsive center-block" alt="Bruschetta Interface with pic of brushetta in android app and a title">

				<h4>Bruschetta Interface Two</h4>
				<img src="a2-2.png" class="img-responsive center-block" alt="no image of bruschetta but second application interface displaying the recipe in align text">
				
				<?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
