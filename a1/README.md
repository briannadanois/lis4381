Brianna Danois

LIS4381 - Mobile Web Application Development 

Assignment 1 README.md

*Git Command Descriptions*
	*1. Git init - initializes a git repo locally
	*2. Git status - checks for untracked and new files/directories in the repo
	*3. Git add - adds those untracked files by marking the changed files
	*4. Git commit - commits them to the local directory
	*5. Git push - pushed the local repo to the remote repo
	*6. Git pull - pulls the remote repo locally
	*7. git checkout -b -creates and switches to a new branch

*Screenshot of Ampps running http://localhost*
![Ampps](ampps.png)

*Screenshot of java Hello*
![Java](hello-java.png)

*Screenshot of Android Studio - My First App*
![Android App] (android-app.png)

*Bitbucket Link to Bitbucket Station Location*
* [A1: Bitbucket Station Location Tutorial Link](https://bitbucket.org/briannadanois/bitbucketlocationstations/ "A1: Bitbucket Station Location Tutorial Link")

*Bitbucket Link to Bitbucket LI4381 Repository
* [A1: Bitbucket LIS4381 Repository Link](https://bitbucket.org/briannadanois/lis4381/ "A1: Bitbucket LIS4381 Repository Link")