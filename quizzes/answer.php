<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio is the compilation of assignments I have created in my LIS4381: Mobile Web Application Development Class.">
	<meta name="author" content="Brianna Danois">
	<link rel="icon" href="../favicon.ico">

	<title>LIS4381 - Quiz 12 Skillset</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
                        <h1>Simple Calculator</h1>
                        <h6>Performs Addition, Subtraction, Multiplication, Division and Exponentiation</h6>
					</div>
                    <div id='answer'>
                        <?php
							$result1 = $POST['fnum'];
							$result2= $_POST['snum'];
							$sign = $_POST['add'];

							if ($sign=='add'){
								echo "Hello!";
							}
							elseif ($sign=='subtract'){
								echo "Hi!";

							}
						?> <?php include_once "../global/footer.php"; ?>
					</div>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

</body>
</html>