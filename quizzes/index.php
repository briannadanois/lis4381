<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio is the compilation of assignments I have created in my LIS4381: Mobile Web Application Development Class.">
	<meta name="author" content="Brianna Danois">
	<link rel="icon" href="../favicon.ico">

	<title>LIS4381 - Quiz 12 Skillset</title>
		<?php include_once("../css/include_css.php"); ?>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
                        <h1>Simple Calculator</h1>
                        <h6>Performs Addition, Subtraction, Multiplication, Division and Exponentiation</h6>
					</div>

					<h2>Perform Calculation</h2>

						<form id="defaultForm" method="post" class="form-horizontal" action="#">
								<div class="form-group">
										<label class="col-8 control-label">Number 1:</label>
										<div class="col-8">
												<input type="text" class="form-control" maxlength:="30" name="fnum" />
										</div>
								</div>

								<div class="form-group">
									<label class="col-8 control-label">Number 2:</label>
									<div class="col-8">
										<input type="text" class="form-control" maxlength="30" name="snum" />
									</div>
                                </div>
                                <br>
                                <input type="radio" name="sign" value='add'>addition
                                <input type="radio" name="sign" value='subtract'>subtraction
								<input type="radio" name="sign" value='multiply'>multiplication
								<input type="radio" name="sign" value='divison'>division
								<input type="radio" name="sign" value='exponent'>exponentiation
                                <br><br><br>

                                <input type="button" value="Select" class="submit" id="submit" 
                                onClick="document.location.href='answer.php'" />

                                
						</form>

			<?php include_once "../global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
			<?php include_once("../js/include_js.php"); ?>

</body>
</html>