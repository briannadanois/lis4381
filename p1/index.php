<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
        <link rel="icon" href="../favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("../global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Project 1 consisted of creating a business card in order to tell future employers about ourselves all through an app. We also enforced our Android Studio skills by creating this useful app.
				</p>

				<h4>Business Card Interface One</h4>
				<img src="a4-1.png" class="img-responsive center-block" alt="interface on android with headshot and my name">

				<h4>Business Card Interface Two</h4>
				<img src="a4-2.png" class="img-responsive center-block" alt="interface on android with my education, interest and contact information">
                
                <?php include_once "../global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
