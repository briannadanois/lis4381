######Brianna Danois

######LIS4381 - Mobile Web Application Development 

####Project 2 README.md

*Main Page Portfolio Carousel*
![Verified](img/caro.png)

*Database Table View*
![Verified](img/p1-1.png)

*Edit Add Form Populated*
![Verified](img/p2-6.png)

*Serverside Validation Error Message*
![Verified](img/p2-3.png)

*Environmental RSS Feed*
![Verified](img/p2-4.png)