<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates BootStrap.">
	<meta name="author" content="Brando Caveiro">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Simple Calculator</title>
		<?php include_once("../css/include_css.php"); ?>

		<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">


</head>
<body>

	<?php include_once("../global/nav.php"); ?>


	<div class="container">
		<div class="starter-template">
					
					<div class="page-header">
						<?php include_once("../global/header.php"); ?>	
					</div>

					<h2>Perform Calculation</h2>
					
<br>


						<form id="default_form"  method="post" class="form-horizontal" action="#">
								<div class="form-group">
									 <label class="col-sm-3 control-label">Number1:</label>
										<div class="col-sm-5">
												<input type="text" class="form-control"  name="num1" /><br>
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-3 control-label">Number2:</label>
										<div class="col-sm-5">
												<input type="text" class="form-control"  name="num2" />
										</div>
								</div>

								<input type=radio name=oper value="add">Addition <input type=radio name=oper value="subtract">Subtraction <input type=radio name=oper value="divide">Division <input type=radio name=oper value="multiply">Multiplication <input type=radio name=oper value="exponentiation">Exponentiation
								<br>	<input type=submit value="Calculate"> <br>


				<?php	include "now.php" ?>
			
						</form>

			<?php include_once "../global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->

	
	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->

<script src="https://code.jquery.com/jquery-3.0.0.min.js" integrity="sha256-JmvOoLtysmqlswxa7mdslmwa6dz9rridtrrvyrndrh0=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0msbjdehialfmubbqp6a4qrprq5ovfw37prr3j5elqxss1yvqotnepnhvp9aj7xs=" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/formValidation/formValidation.min.js"></script>
<script src="js/ie10-viewport-bug-workaround.js"></script>			
			<?php include_once("../js/include_js.php"); ?>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
 $(document).ready(function() {

	$('#default_form').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
					Num1: {
							validators: {
									notEmpty: {
									 message: 'Insert Number 1'
									},
									stringLength: {
											min: 1,
											max: 30,
									 message: 'Insert a number '
									},
									regexp: {
										//alphanumeric, hyphens, underscores, and spaces
										//regexp: /^[a-zA-Z0-9\-_\s]+$/,										
										//similar to: (though, \w supports other Unicode characters)
											regexp: /^[0-9]+$/,
										message: 'Name can only contain letters, numbers, hyphens, and underscore'
									},									
							},
					},

					Num2: {
							validators: {
									notEmpty: {
											message: 'Insert Number 2'
									},
									stringLength: {
											min: 1,
											max: 30,
											message: 'Insert a number'
									},
									regexp: {
										//street: only letters, numbers, commas, hyphens, space character, and periods
										regexp: /^[0-9]+$/,		
									message: 'Street can only contain letters, numbers, commas, hyphens, or periods'
									},									
							},
					},

				
				

   
}

			
					
			}
	});
});
</script>

</body>
</html>
