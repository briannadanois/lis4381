<!DOCTYPE html>
<html lang="en">
<head>
<!--
				 "Time-stamp: <Sun, 05-27-18, 19:34:59 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My Simple Calculator.">
	<meta name="author" content="DeVon Singleton!">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Simple Calculator</title>
		<?php include_once("../css/include_css.php"); ?>

		<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
</head>

<body>
<?php include_once("../global/nav.php"); ?>
  
<?php


// Defining the "calc" class
class calc {
     var $number1;
     var $number2;

          function add($number1,$number2)
          {
                   $result =$number1 + $number2;
                   echo("Addition<br>") ;
  
                    echo("$number1 + $number2 = $result");
                    exit;
           }

          function subtract($number1,$number2)
          {
                   $result =$number1 - $number2;
                   echo("Subtraction<br>"); 
                      echo("$number1 - $number2 = $result");
                    exit;
           }

          function divide($number1,$number2)
          {
               if ($number2 =="0"){echo ("Cannot divide by zero.");}

              else{
                $result =$number1 / $number2;
                   echo("Division<br>"); 
               
                    echo("$number1 ÷ $number2 = $result");
                    exit;}
           }

          function multiply($number1,$number2)
          {
                   $result =$number1 * $number2;
                   echo("Multiplication<br>"); 
                      echo("$number1 x $number2 = $result");
                    exit;
           }
           function exponentiation($number1,$number2)
          {
                   $result = pow($number1, $number2);
                   echo("Exponetiation<br>"); 
  
                    echo("$number1 raised to the $number2 power = $result");
                    exit;
           }
}
$calc = new calc();
?>
<?php

     $number1 = ($_POST['num1']);
     $number2 = ($_POST['num2']);
     $oper = ($_POST['oper']);
     
     if($oper == "add"){
          $calc->add($number1,$number2);
     }
     if($oper == "subtract"){
          $calc->subtract($number1,$number2);
     }
     if($oper == "divide"){
          $calc->divide($number1,$number2);
     }
     if($oper == "multiply"){
          $calc->multiply($number1,$number2);
     }
     if($oper == "exponentiation"){
          $calc->exponentiation($number1,$number2);
     }

     else
      echo"Please select an operation";
      ?> </body>
      </html>